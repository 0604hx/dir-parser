package org.nerve.dir;

import org.junit.Test;
import org.nerve.utils.DateUtils;

import java.io.File;
import java.util.Date;

/**
 * org.nerve.dir
 * Created by zengxm on 4/17/2017.
 */
public class ParseManagerTest {

	@Test
	public void parseWithZhCN() throws Exception {
		File file = new File(getFile("dir_c_zh-cn.txt"));

		ParseManager manager = new ParseManager(file.getAbsolutePath());
		manager.start();

		System.out.println("共得到 Content : "+manager.getPickCount());
	}

	@Test
	public void parseWithZhCN2() throws Exception {
		File file = new File(getFile("dir_c_zh-cn-2.txt"));

		ParseManager manager = new ParseManager(file.getAbsolutePath());
		manager.start();

		System.out.println("共得到 Content : "+manager.getPickCount());
	}

	@Test
	public void parseWithEN()throws Exception{
		File file = new File(getFile("dir_c_windows_en.txt"));

		ParseManager manager = new ParseManager(file.getAbsolutePath());
		manager.start();

		System.out.println("共得到 Content : "+manager.getPickCount());
	}

	@Test
	public void parseDate(){
		System.out.println(DateUtils.formatDate(new Date(1556835240000L),"MM/dd/yyyy hh:mm a"));
	}

	public String getFile(String name){
		return getClass().getClassLoader().getResource(name).getFile();
	}
}
