package org.nerve.dir;

import java.io.Serializable;

/**
 * 文件对象
 * com.zeus.ircp.domain.forensics
 * Created by zengxm on 11/29/2016.
 */
public class Content implements Serializable{

	private String name;                //内容名称，文件/目录的话就是文件名，驱动器的话就是驱动盘的名称
	private String path;                //文件系统中的完整父目录地址（不带当前文件名）
	private String pathMd5;             //由于path字段是text（因为文件路径可能会很长），不能用于unique的主键，所以对path进行md5加密，用于unique限制
	private ContentType contentType=ContentType.FILE;               //文件类别，文件或者目录，默认为File
	private long size;                  //内容大小，单位b
	private long freeSize;              //空闲空间大小，单位b，只针对驱动器
	private boolean readOnly;           //是否只读
	private long creationTime;          //创建时间。使用的是13位数值时间戳
	private long lastWriteTime;         //最后修改时间。使用的是13位数值时间戳
	private long lastAccessTime;        //最后访问时间。使用的是13位数值时间戳

	/**
	 * 获取完整的路径
	 * @return
	 */
	public String fullPath(){
		if(getPath() == null)
			return getName();
		//计算目录分隔符，判断path是否包含了/
		if(path.contains("/"))
			return String.join("/", path, name).replaceAll("['/']+","/");
		else
			return String.join("\\", path, name).replaceAll("['\\\\']+","\\\\");
	}

	public String getName() {
		return name;
	}

	public Content setName(String name) {
		this.name = name;
		return this;
	}

	public String getPath() {
		return path;
	}

	public Content setPath(String path) {
		this.path = path;
		return this;
	}

	public String getPathMd5() {
		return pathMd5;
	}

	public Content setPathMd5(String pathMd5) {
		this.pathMd5 = pathMd5;
		return this;
	}

	public ContentType getContentType() {
		return contentType;
	}

	public Content setContentType(ContentType contentType) {
		this.contentType = contentType;
		return this;
	}

	public long getSize() {
		return size;
	}

	public Content setSize(long size) {
		this.size = size;
		return this;
	}

	public long getFreeSize() {
		return freeSize;
	}

	public Content setFreeSize(long freeSize) {
		this.freeSize = freeSize;
		return this;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public Content setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
		return this;
	}

	public long getCreationTime() {
		return creationTime;
	}

	public Content setCreationTime(long creationTime) {
		this.creationTime = creationTime;
		return this;
	}

	public long getLastWriteTime() {
		return lastWriteTime;
	}

	public Content setLastWriteTime(long lastWriteTime) {
		this.lastWriteTime = lastWriteTime;
		return this;
	}

	public long getLastAccessTime() {
		return lastAccessTime;
	}

	public Content setLastAccessTime(long lastAccessTime) {
		this.lastAccessTime = lastAccessTime;
		return this;
	}

	@Override
	public String toString() {
		return "Content{" +
				"name='" + name + '\'' +
				", path='" + path + '\'' +
				", pathMd5='" + pathMd5 + '\'' +
				", contentType=" + contentType +
				", size=" + size +
				", freeSize=" + freeSize +
				", readOnly=" + readOnly +
				", creationTime=" + creationTime +
				", lastWriteTime=" + lastWriteTime +
				", lastAccessTime=" + lastAccessTime +
				'}';
	}
}
