package org.nerve.dir.impl;

import org.apache.commons.lang3.StringUtils;
import org.nerve.dir.Content;
import org.nerve.dir.IPicker;

/**
 * org.nerve.dir.impl
 * Created by zengxm on 4/17/2017.
 */
public class ConsolePicker implements IPicker {
	protected long count = 0;
	@Override
	public void onData(Content content) throws Exception {
		System.out.println(String.format("[ConsolePick] %d. %s ",count++,content));
	}
}
