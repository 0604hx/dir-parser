package org.nerve.dir;

/**
 * 负责Content的入库
 * Created by zengxm on 2015/7/2 0002.
 */
public interface IPicker {

	/**
	 * 当数据实体解析成功后，会调用此方法
	 * @param content
	 * @return
	 */
	void onData(Content content) throws Exception;
}