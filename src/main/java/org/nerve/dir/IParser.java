package org.nerve.dir;


/**
 * 通用解析器接口
 *
 * com.nerve.jiepu.forensicsystem.parser
 * Created by zengxm on 2015/7/2 0002.
 */
public interface IParser {
	/**
	 * 从data中解析出Content实体
	 * @param line
	 * @return
	 * @throws Exception
	 */
	Content parse(String line) throws Exception;

	/**
	 * 返回解析器的名称
	 * @return
	 */
	String getName();

	/**
	 * 读取文件时的编码
	 * @return
	 */
	String getEncoding();
}
