package org.nerve.dir.exception;

/**
 * com.nerve.jiepu.forensicsystem.parser.exception
 * Created by zengxm on 2015/7/2 0002.
 */
public class PickerException extends RuntimeException {

	public PickerException(){
		super();
	}

	public PickerException(String message){
		super(message);
	}
}
