package org.nerve.dir.exception;

/**
 * com.nerve.jiepu.forensicsystem.parser.exception
 * Created by zengxm on 2015/7/2 0002.
 */
public class ParserException extends RuntimeException {

	public ParserException(){
		super();
	}

	public ParserException(String message){
		super(message);
	}
}