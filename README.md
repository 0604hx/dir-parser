# windows DIR 命令解析器

> 针对 `dir` 命令输出结果的解析器

## 实体结构

```java
private String name;                //内容名称，文件/目录的话就是文件名，驱动器的话就是驱动盘的名称
private String path;                //文件系统中的完整父目录地址（不带当前文件名）
private String pathMd5;             //由于path字段是text（因为文件路径可能会很长），不能用于unique的主键，所以对path进行md5加密，用于unique限制
private ContentType contentType=ContentType.FILE;               //文件类别，文件或者目录，默认为File
private long size;                  //内容大小，单位b
private long freeSize;              //空闲空间大小，单位b，只针对驱动器
private boolean readOnly;           //是否只读
private long creationTime;          //创建时间。使用的是13位数值时间戳
private long lastWriteTime;         //最后修改时间。使用的是13位数值时间戳
private long lastAccessTime;        //最后访问时间。使用的是13位数值时间戳
```

## LOGS

### 2017年4月17日

初步实现解析功能。
